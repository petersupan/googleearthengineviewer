/*
    src/example1.cpp -- C++ version of an example application that shows 
    how to use the various widget classes. For a Python implementation, see
    '../python/example1.py'.

    NanoGUI was developed by Wenzel Jakob <wenzel@inf.ethz.ch>.
    The widget drawing code is based on the NanoVG demo application
    by Mikko Mononen.

    All rights reserved. Use of this source code is governed by a
    BSD-style license that can be found in the LICENSE.txt file.
*/

#define NANOVG_GLES2_IMPLEMENTATION

#include <emscripten.h>

#if defined(_WIN32)
#include <windows.h>
#endif
#include <iostream>

    #include <SDL2/SDL.h>
    #include <SDL2/SDL_opengl.h>

//#include <opencv2/opencv.hpp>

//#include "SDL_image.h"
//#include "SDL2/SDL2_gfxPrimitives.h"
#include <vector>

#include <map>
#include <string>
#include <list>
#include <assert.h>

#include "Viewer.h"
#include "Cache.h"
#include "TileProvider.h"

using std::cout;
using std::cerr;
using std::endl;

#undef main


SDL_Event e;

SDL_Window *window;        // Declare a pointer to an SDL_Window
SDL_Renderer* renderer;

bool quit = false;

unsigned int texId;

int winWidth = 1024;
int winHeight = 768;

void getImageFromUrl(std::string url) {

}

class MouseHand {
public:
    bool pressed;
    int prevx;
    int prevy;
    // position in pixels how much has been moved
    int posx;
    int posy;
    int diffx;
    int diffy;
    void setPosition(int x, int y) {posx=x; posy=y;}
    void handle(SDL_Event* e) {
        if (e->type == SDL_MOUSEBUTTONDOWN) {
            SDL_MouseButtonEvent* mbe = reinterpret_cast<SDL_MouseButtonEvent*>(e);
            prevx = mbe->x;
            prevy = mbe->y;
            pressed = true;
        } else if (e->type == SDL_MOUSEBUTTONUP) {
            pressed = false;

        } else if (e->type == SDL_MOUSEMOTION) {
            if (pressed) {
                SDL_MouseMotionEvent* mme = reinterpret_cast<SDL_MouseMotionEvent*>(e);
                posx -= mme->xrel;
                posy -= mme->yrel;
                diffx += mme->xrel;
                diffy += mme->yrel;
            }
        }
    }

    MouseHand() :
        pressed(false),
        prevx(0),
        prevy(0),
        posx(0),
        posy(0),
        diffx(0),
        diffy(0)
    {}
};

MouseHand* mh;
Viewer* v;
TileProvider* tileP;

//void mainloop(void* arg) {
void mainloop() {
    static int nrCalls = 0;
    nrCalls++;
    //printf("starting mainloop the %ith time\n", nrCalls);
            while( SDL_PollEvent( &e ) != 0 )
            {
//                printf("another event %i", e.type);

                //User requests quit
                if( e.type == SDL_QUIT )
                {
                    quit = true;
                }
                if(e.type == SDL_KEYUP) {
                    quit = true;
                }

                mh->handle(&e);
            }
            //printf("after event loop\n");


            //v->setPos(mh->posx, mh->posy);
            v->setPixelDiff(-mh->diffx, -mh->diffy);
            mh->diffx = 0; mh->diffy = 0;

            SDL_RenderClear(renderer);
            //printf("after clear\n");

            SDL_Rect srcR;
            SDL_Rect dstR;
            srcR.x= 0; srcR.y =0; srcR.w=256; srcR.h=256;

            //printf("before tielloop\n");
            std::vector<TilePos>* tiles = v->getNeededTiles();
            for(int i = 0; i < tiles->size(); i++){
                TilePos p = (*tiles)[i];
                dstR.x= p.posX; dstR.y =p.posY; dstR.w=256; dstR.h=256;

                //SDL_Surface* s = tileP.getTile(p.tx, p.ty, 8);
                //SDL_Texture* t = tileP->getTile(p.tx, p.ty, 8);
                SDL_Texture* t = tileP->getTile(p.tx, p.ty, round(v->zoom));

                SDL_RenderCopy(renderer, t, &srcR, &dstR);
            }



            SDL_RenderPresent(renderer);
}


int main(int  argc , char **  argv )
{

    SDL_Init(SDL_INIT_VIDEO);   // Initialize SDL2
    //IMG_Init(IMG_INIT_PNG); //Initialize sdl2 image

    for (int i = 0; i < argc; i++) {
        printf("AAAAargv[%i]: %s\n", i, argv[i]);
        printf("starting urls concat");
    }

    std::string mapIdStr;
    std::string tokenStr;
    if (argc > 3) {
        mapIdStr = argv[4];
        tokenStr = argv[2];
    }

//    if (argc > 3) {
//        printf("starting urls concat\n");
//        std::string baseUrl = "https://earthengine.googleapis.com/map";
//        std::string mapIdStr = argv[4];
//        std::string tokenStr = argv[2];
//        std::string zoom="8";
//        std::string tilex="232";
//        std::string tiley="154";
//        std::string url = baseUrl +"/"+ mapIdStr +"/"+ zoom +"/"+ tilex +"/"+ tiley +"?token="+tokenStr;
//        printf("The Url string is: %s\n", url.c_str());
//        //emscripten_wget(url.c_str(), "testimage.png");
//        //srf = IMG_Load("testimage.png");
//        //printf("The loaded image: %i / %i\n", srf->w, srf->h);
//    } else {
//        //srf = SDL_CreateRGBSurface(0, 256, 256, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
//        SDL_Rect rect;
//        rect.x =50; rect.y=40; rect.w=100; rect.h=80;
//        //SDL_FillRect(srf, &rect, 0xff0000ff);
//    }


//#ifdef NANOVG_GL2_IMPLEMENTATION
//    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
//    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,2);
//    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,0);
//#elif defined(NANOVG_GL3_IMPLEMENTATION)
//    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
//    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,3);
//    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,3);
//#elif defined(NANOVG_GLES2_IMPLEMENTATION)
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,0);
//#elif defined(NANOVG_GLES3_IMPLEMENTATION)
//    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
//    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION,3);
//    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION,0);
//#endif
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);


    // Create an application window with the following settings:
    window = SDL_CreateWindow(
      "An SDL2 window",         //    const char* title
      SDL_WINDOWPOS_UNDEFINED,  //    int x: initial x position
      SDL_WINDOWPOS_UNDEFINED,  //    int y: initial y position
      winWidth,                      //    int w: width, in pixels
      winHeight,                      //    int h: height, in pixels
      SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN          //    Uint32 flags: window options, see docs
    );

    // Check that the window was successfully made
    if(window==NULL){
      // In the event that the window could not be made...
      std::cout << "Could not create window: " << SDL_GetError() << '\n';
      SDL_Quit();
      return 1;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);
    //renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);//SDL_RENDERER_ACCELERATED);

    mh = new MouseHand();
    v = new Viewer();
    tileP = new TileProvider(renderer);
    tileP->token = tokenStr;
    tileP->mapId = mapIdStr;

    emscripten_set_main_loop((em_callback_func)mainloop, -1, 1);

//    while(!quit) {
//        mainloop();

//    }


    return 0;
}
