#ifndef _TILEDOWNLOADER_H_
#define _TILEDOWNLOADER_H_

#include <string>
#include <set>

class SDL_Texture;
class SDL_Surface;
class SDL_Renderer;
class TileProvider;

class TileDownloader {
public:
    void dlTile(TileProvider* p, int x, int y, int zoom, std::string &mapidStr, std::string &tokenStr);
    static void onLoadWget(const char* fname);
    static void onErrorWget(const char* fname);

    std::string justGetFn(int x, int y, int zoom, std::string &mapidStr);
private:
    static TileProvider* p;
    static std::set<std::string> inflightImages;
};




#endif
