#include "Viewer.h"
#include "math.h"
#include <string>
#include <emscripten.h>

#define DEG2RAD(a)   ((a) / (180 / M_PI))
#define RAD2DEG(a)   ((a) * (180 / M_PI))
#define EARTH_RADIUS 6378137

/* The following functions take their parameter and return their result in degrees */

double y2lat_d(double y)   { return RAD2DEG( atan(exp( DEG2RAD(y) )) * 2 - M_PI/2 ); }
double x2lon_d(double x)   { return x; }

double lat2y_d(double lat) { return RAD2DEG( log(tan( DEG2RAD(lat) / 2 +  M_PI/4 )) ); }
double lon2x_d(double lon) { return lon; }

/* The following functions take their parameter in something close to meters, along the equator, and return their result in degrees */

double y2lat_m(double y)   { return RAD2DEG(2 * atan(exp( y/EARTH_RADIUS)) - M_PI/2); }
double x2lon_m(double x)   { return RAD2DEG(              x/EARTH_RADIUS           ); }

/* The following functions take their parameter in degrees, and return their result in something close to meters, along the equator */

double lat2y_m(double lat) { return log(tan( DEG2RAD(lat) / 2 + M_PI/4 )) * EARTH_RADIUS; }
double lon2x_m(double lon) { return          DEG2RAD(lon)                 * EARTH_RADIUS; }

double lat2yTile(double lat, int zoom) {
    double n = pow(2.0, zoom)*256.0;
    double lat_rad = DEG2RAD(lat);
    double yPix = n * (1.0 - (log(tan(lat_rad)+ (1.0/cos(lat_rad)) )/M_PI)) / 2.0;
    return yPix;
}

float Viewer::getPixelY(){
    double pixl = lat2yTile(yPos, zoom);
    return pixl;
}


std::vector<TilePos>* Viewer::getNeededTiles() {
    neededTiles.clear();
    int minTileX = getPixelX()/256.0;//floor(xPos);
    int minTileY = getPixelY()/256.0;//floor(yPos);

    int maxTileX = (getPixelX() + windowW)/256.0;
    int maxTileY = (getPixelY() + windowH)/256.0;


    for (int y = minTileY; y <=maxTileY; y++) {
        for(int x = minTileX; x <= maxTileX; x++) {
            int tPX = x * 256 -getPixelX();
            int tPY = y * 256 -getPixelY();
            neededTiles.push_back(TilePos(x,y, tPX, tPY));
        }
    }

    return &neededTiles;
}

void Viewer::showLatLng() {
    //std::string jsText = "var latField = document.getElementById('lat'); \
                        //var longField = document.getElementById('long'); \
                        //latField.value='blibla'; longField.value='blub'";
    EM_ASM_({
        var latField = document.getElementById('lat');
        var longField = document.getElementById('long');
        latField.value=$0; longField.value=$1;
    }, xPos, yPos);
    //emscripten_run_script(jsText.c_str());
}
