#include "Cache.h"
#include <assert.h>


Cache::Cache() {
    maxEntries=10;
}

void Cache::setEntry(std::string key, int entry) {
    // assume this entry does not exist yet
    auto mit = dict.find(key);
    if (mit != dict.end()) {
        assert(false);
    }

    std::pair<std::string, int> newListEntry(key, entry);
    list.push_front(newListEntry);
    std::pair<std::string, decltype(list.begin())> newDictEntry(key, list.begin());
    dict.insert(newDictEntry);
}

int Cache::popOldestEntry() {
    auto lit = list.back();
    std::string keyToErase = lit.first;
    int idx = lit.second;
    auto mitToErase = dict.find(keyToErase);
    dict.erase(mitToErase);
    list.pop_back();
    return idx;
}

int Cache::getEntry(std::string key) {
    if (dict.find(key) != dict.end()) {
        auto lit = dict[key];
        int idx = lit->second;
        list.erase(lit);
        std::pair<std::string, int> newListEntry(key, idx);
        list.push_front(newListEntry);
        dict[key] = list.begin();
        return idx;
    } else {
        assert(false);
        return -1;
    }
}

bool Cache::hasEntry(std::string key) {
    if (dict.find(key) != dict.end()) {
        return true;
    } else {
        return false;
    }
}
