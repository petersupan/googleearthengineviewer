#ifndef _TILEPROVIDER_H_
#define _TILEPROVIDER_H_

#include "Cache.h"
#include "TileDownloader.h"

class SDL_Texture;
class SDL_Surface;
class SDL_Renderer;
class TileDownloader;

class TileProvider {
public:
    SDL_Texture* getTile(int x, int y, int zoom);
    TileProvider(SDL_Renderer* _renderer);
    
    void setTile(std::string key, SDL_Surface* srf);

    std::string mapId;
    std::string token;
private:
    SurfaceCache<SDL_Surface*> sc;
    SurfaceCache<SDL_Texture*> tc;
    SDL_Renderer* renderer;
    TileDownloader dl;
    char s[30];

    void requestTile(int x, int y, int zoom) ;
    SDL_Texture* defaultTile;
    SDL_Texture* surfaceToTex(SDL_Surface* srf);
    void copySurfaceToTex(SDL_Surface* srf, SDL_Texture* tex);
    void createDefaultTile();

};




#endif
