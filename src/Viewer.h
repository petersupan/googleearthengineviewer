#ifndef _VIEWER_H_
#define _VIEWER_H_

#include <vector>
#include <math.h>

struct TilePos {
    //tile id
    int tx;
    int ty;
    // pixel position on screen
    int posX;
    int posY;
    TilePos(int _tx, int _ty, int _posX, int _posY) :
        tx(_tx),
        ty(_ty),
        posX(_posX),
        posY(_posY) {

    }
};

class Viewer {
public:
    // position in lat long
    double xPos;
    double yPos;
    float zoom;

    int windowW;
    int windowH;

    Viewer() {
        //position in lat/long
        xPos= 17.57;
        yPos=46.92;
        zoom=4.0;
        windowW = 1024;
        windowH = 768;
        neededTiles.reserve(50);
    }

//    void setPos(int x, int y) {
//        xPos = (float)x/256.0;
//        yPos = (float)y/256.0;
//    }

    void setPixelDiff(int dx, int dy) {
        double nrXPixels = pow(2, round(zoom)+8 );
        double nrYPixels = pow(2, round(zoom)+8);
        double xPixelSize = 360.0/nrXPixels;
        double yPixelSize = ((85.05112878)*2)/nrYPixels;
        xPos += (float)dx*xPixelSize;
        yPos -= (float)dy*yPixelSize;
        showLatLng();
    }

    void showLatLng();

    float getPixelX(){
        double nrXPixels = pow(2, round(zoom)+8 );
        double pixPerLatUnit = nrXPixels/360.0;
        return (xPos+180.0) * pixPerLatUnit;
    }
    float getPixelY();

    std::vector<TilePos>* getNeededTiles();
    std::vector<TilePos> neededTiles;
};



#endif
