#include "TileProvider.h"

#include <SDL2/SDL.h>


TileProvider::TileProvider(SDL_Renderer *_renderer) : renderer(_renderer){
    createDefaultTile();
    sc.maxEntries = 40;
    tc.maxEntries = 30;
    SDL_Surface* srf;
    srf = SDL_CreateRGBSurface(0, 256, 256, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
    SDL_Renderer* r = SDL_CreateSoftwareRenderer(srf);
    SDL_Rect fR; fR.x=0;fR.y=0;fR.w=240;fR.h=240;
    SDL_FillRect(srf, &fR, 0x0000ffff);
    //boxColor(r, 0, 0, 256, 256, 0x00ff00ff);
    std::string text = "this tile is uninitialized";
    //stringColor(r, 20, 20, text.c_str(), 0xffffffff);
    for(int i = 0; i < 30; i++) {
        SDL_Texture* tex = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, 256, 256);
        std::string text = std::string("dummy") + std::to_string(i);
        tc.addEntry(text, tex);
    }
}

void TileProvider::copySurfaceToTex(SDL_Surface* srf, SDL_Texture* tex) {
    SDL_Rect fR; fR.x=0;fR.y=0;fR.w=256;fR.h=256;
    unsigned char* pixelDest; int pitch;
    int ret = SDL_LockTexture(tex, &fR, (void**)(&pixelDest), &pitch);
    if (ret< 0) {
        // Unrecoverable error, exit here.
        const char* error = SDL_GetError();
        printf("locktexture failed: %s\n", error);
    }
    unsigned char* srfPix = (unsigned char*) srf->pixels;
    for(int i = 0; i < 256; i++) {
        memcpy((pixelDest)+(pitch)*i, srfPix+srf->pitch*i, 256*4);
    }
    SDL_UnlockTexture(tex   );
}

SDL_Texture* TileProvider::surfaceToTex(SDL_Surface* srf) {
    SDL_Texture* tex = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ABGR8888, SDL_TEXTUREACCESS_STREAMING, 256, 256);
    copySurfaceToTex(srf, tex);
    return tex;
}

void TileProvider::createDefaultTile() {
    SDL_Surface* srf = SDL_CreateRGBSurface(0, 256, 256, 32, 0xff000000, 0x00ff0000, 0x0000ff00, 0x000000ff);
    SDL_Renderer* r = SDL_CreateSoftwareRenderer(srf);
    if (!r) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Render creation for surface fail : %s\n",SDL_GetError());
    }
    SDL_Rect fR; fR.x=10;fR.y=10;fR.w=246;fR.h=246;
    
    unsigned int color = 0x0000aaff;
    
    SDL_FillRect(srf, &fR, color);
    
    defaultTile = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, 256, 256);
    fR.x=0;fR.y=0;fR.w=256;fR.h=256;
    unsigned char* pixelDest; int pitch;
    int ret = SDL_LockTexture(defaultTile, &fR, (void**)(&pixelDest), &pitch);
    if (ret< 0) {
        // Unrecoverable error, exit here.
        const char* error = SDL_GetError();
        printf("locktexture failed: %s\n", error);
    }
    unsigned char* srfPix = (unsigned char*) srf->pixels;
    for(int i = 0; i < 256; i++) {
        memcpy((pixelDest)+(pitch)*i, srfPix+srf->pitch*i, 256*4);
    }
    SDL_UnlockTexture(defaultTile);
}

void TileProvider::requestTile(int x, int y, int zoom) {
    dl.dlTile(this, x,y, zoom, mapId, token);
}

void TileProvider::setTile(std::string key, SDL_Surface* srf) {
    sc.addEntry(key, srf);
}

SDL_Texture* TileProvider::getTile(int x, int y, int zoom) {
    //std::string key = std::to_string(x)+ "_"+std::to_string(y) + "_"+std::to_string(zoom);
    std::string key = dl.justGetFn(x, y,zoom, mapId);
    if (tc.hasEntry(key)) {
        return tc.getEntry(key);
    } else {

        //printf("cache miss %s\n", key.c_str());
        SDL_Surface* srf;
        if (sc.hasEntry(key)) {
            srf = sc.getEntry(key);
            SDL_Texture* tex = tc.replaceOldestEntry(key);
            copySurfaceToTex(srf, tex);
            
//            SDL_Texture* tex = tc.replaceOldestEntry(key);
//            SDL_Rect fR; fR.x=0;fR.y=0;fR.w=256;fR.h=256;
//            unsigned char* pixelDest; int pitch;
//            int ret = SDL_LockTexture(tex, &fR, (void**)(&pixelDest), &pitch);
//            if (ret< 0) {
//                // Unrecoverable error, exit here.
//                const char* error = SDL_GetError();
//                printf("locktexture failed: %s\n", error);
//            }
//            unsigned char* srfPix = (unsigned char*) srf->pixels;
//            for(int i = 0; i < 256; i++) {
//                memcpy((pixelDest)+(pitch)*i, srfPix+srf->pitch*i, 256*4);
//            }
//            SDL_UnlockTexture(tex);
            return tex;
        } else {
            requestTile(x, y, zoom);
            return defaultTile;
        }
    }
}
