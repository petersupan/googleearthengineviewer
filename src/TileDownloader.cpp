#include "TileDownloader.h"

#include <emscripten.h>

#include <SDL2/SDL.h>
#include "SDL_image.h"
#include "TileProvider.h"
#include "unistd.h"
//#include <sys/stat.h>
#include <cstdio>

TileProvider* TileDownloader::p;
std::set<std::string> TileDownloader::inflightImages;

int exists(const char *fname)
{
    FILE *file;
    if ((file = fopen(fname, "r")))
    {
        fclose(file);
        return 1;
    }
    return 0;
}

std::string TileDownloader::justGetFn(int x, int y, int zoom, std::string &mapidStr) {
    std::string zoomStr=std::to_string(zoom);
    std::string tilex=std::to_string(x);
    std::string tiley=std::to_string(y);
    std::string fn = "/" +mapidStr+"_"+zoomStr+"_"+tilex+"_"+tiley+".png";
    return fn;
}

void TileDownloader::dlTile(TileProvider *p, int x, int y, int zoom, std::string &mapidStr, std::string &tokenStr){
    TileDownloader::p = p;
    static int nrCalls = 0;
    nrCalls++;
    //printf("starting urls concat the %ith time\n", nrCalls);
    std::string baseUrl = "https://earthengine.googleapis.com/map";
    std::string zoomStr=std::to_string(zoom);
    std::string tilex=std::to_string(x);
    std::string tiley=std::to_string(y);
    std::string url = baseUrl +"/"+ mapidStr +"/"+ zoomStr +"/"+ tilex +"/"+ tiley +"?token="+tokenStr;
    //printf("The Url string is: %s\n", url.c_str());
    std::string fn = mapidStr+"_"+zoomStr+"_"+tilex+"_"+tiley+".png";
    if (inflightImages.find(fn) == inflightImages.end()) {
        printf("requesting url string: %s\n", url.c_str());
        emscripten_async_wget(url.c_str(), fn.c_str(), onLoadWget, onErrorWget);
        inflightImages.insert(fn);
    } else {
        fn = "/" + fn;
        if (exists(fn.c_str())) {
            printf("file already exists %s\n", fn.c_str());

            onLoadWget(fn.c_str());
        }

    }
}

void TileDownloader::onLoadWget(const char* fname){
    printf("starting loading %s\n", fname);
    SDL_Surface* srf = IMG_Load(fname);
    //std::remove(fname);
    //unlink(fname);
    inflightImages.erase(fname);
    if(inflightImages.find(fname) != inflightImages.end()) {
        printf("this file shouldnt be here %s", fname);
    } else {
        printf("deleted %s", fname);
    }

//    unsigned char* dat = (unsigned char*)srf->pixels;
//    for(int y = 0; y < srf->h;y++) {
//        int startPos = y *256*4;
//        for(int x = 0; x < srf->w;x++) {
//            dat[startPos + x*4+3] = 255;
//        }
//    }

    p->setTile(fname, srf);
 }

void TileDownloader::onErrorWget(const char* fname){
  printf("wget failed: %s", fname);
  inflightImages.erase(fname);
}
