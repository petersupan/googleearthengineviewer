#ifndef _CACHE_H_
#define _CACHE_H_

#include <vector>
#include <map>
#include <list>
#include <string>
#include <assert.h>

class Cache {
public:
    Cache();
    void setEntry(std::string key, int entry);
    int getEntry(std::string key);
    bool hasEntry(std::string key);
    int popOldestEntry();
    int getNrEntries() { return list.size(); }
private:
    std::list<std::pair<std::string, int>> list;
    std::map<std::string, decltype(list.begin())> dict;
    int maxEntries;
};

template <typename V>
class SurfaceCache {
public:
    SurfaceCache() {
        maxEntries = 10;
    }
    bool hasEntry(std::string key) {
        return c.hasEntry(key);
    }
    V getEntry(std::string key) {
        assert(c.getNrEntries() == pool.size());
        int idx = c.getEntry(key);
        //printf("returning pool[%i]\n", idx);
        return pool[idx];
    }
    V replaceOldestEntry(std::string key) {
        int idx = c.popOldestEntry();
        c.setEntry(key, idx);
        return pool[idx];
    }

    void addEntry(std::string key, V entry) {
        assert(c.getNrEntries() == pool.size());
        assert(!c.hasEntry(key));
        if(pool.size() >= maxEntries) {
            int idx = c.popOldestEntry();
            c.setEntry(key, idx);
            pool[idx] = entry;
        } else {
            pool.push_back(entry);
            c.setEntry(key, pool.size()-1);
        }
    }

    int maxEntries;

private:
    Cache c;
    std::vector<V> pool;
};



#endif
